<html>
  <head>
    <title>About Tad Simmons</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <script type="text/javascript" src="javascriptfile.js?2"></script>

  </head>
  <body>
    <?php include("header.php"); ?>
    <div class="aboutStyle">
  		<p>	In October 2011, Tad’s life came to a sudden and tragic end, in a motorcycle accident.
  			Tad was a 2008 graduate of Triad High School, a 2010 graduate of MMI in Arizona, and
  			was pursuing a career as a Union Laborer, following in his father’s footsteps.
  			After Tad’s death, friends and family decided to develop a Memorial Foundation in his honor,
  			to keep his life, and our memories with him alive, promote motorcycle awareness and safety,
  			and provide financial assistance to qualified High School applicants pursuing
  			college/secondary education with a future in helping people.
  		</p>
    </div>
  </body>
</html>
