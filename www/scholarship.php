<html>
  <head>
    <title>Scholarship</title>
    <link rel="stylesheet" type="text/css" href="style.css?" />
    <script type="text/javascript" src="javascriptfile.js?2"></script>

  </head>
  <body>
  <?php include("header.php"); ?>
  <div class="scholarshipStyle">
    <p>2013 Winners:</p>
	    <p>Ben Brendel & Cole Bush</p>
      <img src="./Media/ben_brendel.jpg"/>
      <img src="./Media/cole_bush.jpg"/>
    <p>2014 Winners:</p>
    	<p>Parker Nesbit & Geoff Matthis</p>
      <img src="./Media/parker.jpg"/>
    <p>2015 Winners:</p>
    	<p>Jack Ralston & Andrew Nadler</p>
      <img src="./Media/jack.png"/>
      <img src="./Media/andrew.jpg"/>
    <p>2016 Winners:</p>
    	<p>Matthew Sarhage & Rebecca Kuhl</p>
      <img src="./Media/matthew.jpg"/>
    <p>2017 Winners:</p>
    	<p>Maddie Jenkins and Sarah Harding</p>
      <img src="./Media/maddie_jenkins.jpg"/>
    <p>2018 Winners:</p>
    	<p>Collin Barbour and Rebecca Newman</p>
      <img src="./Media/collin_barbour.jpg"/>
      <br><br>
  </div>
  <?php include("footer.php"); ?>
  </body>
</html>
