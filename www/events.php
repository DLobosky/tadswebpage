<html>
  <head>
    <title>About Tad</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <script type="text/javascript" src="javascriptfile.js?2"></script>

  </head>
  <body>
    <?php include("header.php"); ?>
    <div class="eventStyle">
      <div class="eventInfo">
    		<p>	Sign Up @ Phyl's Chet & Rose's Tavern @ 11 am in Marine, IL </p>
        <p> Available at Phyl's: </p>
        <p> - 50/50 Tickets </p>
        <p> - Silent Auction </p>
        <p> - T-Shirts </p>
      </div>
      <br>
      <p> Stops Include: </p>
      <div class="stopList">
        <p>Tow Bar - St. Jacob, IL </p>
        <p>Bottoms Up - Jamestown, IL</p>
        <p>Motorheads Clubhouse - Alhambra, IL</p>
        <p>Weezy's Route 66 Bar & Grill - Hamel, IL</p>
        <br>
        <div class="rideInfo">
          <p>The ride will end back at Phyl's Tavern in Marine. </p>
          <p>Drawings and silent auction end @ 6 pm.</p>
          <p>All proceeds will benefit the Annual Scholarship Fund of the Tad Simmons Memorial Foundation.</p>
        </div>
      </div>
    </div>
          <?php include("footer.php"); ?>
  </body>
</html>
