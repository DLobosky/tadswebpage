<link rel="stylesheet" type="text/css" href="style.css?" />
<style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
    border-right:1px solid #bbb;
}

li:last-child {
    border-right: none;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover:not(.active) {
    background-color: #111;
}

.active {
    background-color: #4CAF50;
}
</style>

<ul>
  <li><a href="index.php">Home</a></li>
  <li><a href="about.php">About Tad</a></li>
  <li><a href="events.php">Events</a></li>
  <li><a href="tshirts.php">T-Shirts</a></li>
  <li><a href="scholarship.php">Scholarship</a></li>
</ul>

<div class="main">
  <div class="row1">
    <div class=title>
      <h1> 7th Annual Tad Simmons Memorial Ride</h1>
      <h1> Saturday, August 25th, 2018</h1>
    </div>
  </div>

<!--
  <div class="row2">
    <div class="navBar">
      <p>Navigation</p>
      <hr align="left" width="15%">
      <div class="navList">
        <ul>
      		<li><a href="index.php">Home</a></li>
      		<li><a href="tshirts.php">T-Shirts</a></li>
      		<li><a href="about.php">About Tad</a></li>
          <li><a href="events.php">Events</a></li>
          <li><a href="about.php">Scholarship</a></li>
        </ul>
      </div>
    </div>
  </div>
-->
